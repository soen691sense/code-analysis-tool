package org.e.detection;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.MethodCallExpr;
import org.apache.log4j.BasicConfigurator;
import org.core.patterns.ExceptionFinder;
import org.core.visitors.ResolveMethodCalls;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.util.Constant;
import org.util.FilterFile;
import org.util.SaveResultToDisk;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class execute {
    private static Logger logger = LoggerFactory.getLogger(execute.class);

    public static void main(String[] args) throws Exception {
        BasicConfigurator.configure();

        FilterFile filterFile = new FilterFile();
        logger.info("Get all the java files in the path: {}", Constant.TESTFOLDER);
        List<File> files = filterFile.walk(Constant.TESTFOLDER);
        File searchPath = new File(Constant.TESTFOLDER);
        SaveResultToDisk saveResultToDisk = new SaveResultToDisk();
        // create a List which contains String array
        List<String[]> data = new ArrayList<>();
        String[] tmp_data = new String[]{"File Path", "Start Line", "Start Clo", "End Line", "End Col", "Package Name", "Class Name", "Signature"};
        data.add(tmp_data);

        String[] tmp_data2 = new String[]{"File Path", "Start Line", "Start Clo", "End Line", "End Col", "Package Name", "Class Name", "Signature"};
        data.add(tmp_data2);

        saveResultToDisk.writeDataOneByOne(Constant.TESTRESULT, tmp_data);
        saveResultToDisk.writeDataOneByOne(Constant.TESTRESULT2, tmp_data);

        for (File file : files) {
            try {
                logger.info("Now processing file with path: {}", file.getAbsolutePath());
                ExceptionFinder exceptionFinder = new ExceptionFinder();
                ResolveMethodCalls resolveMethodCalls = new ResolveMethodCalls();
                CompilationUnit cu = resolveMethodCalls.getCu(file, searchPath, Constant.TESTJAR);
                CompilationUnit cuNoJar = resolveMethodCalls.getCuWithNoJar(file, searchPath);

                if (cu != null) {
                    for (MethodCallExpr methodCallExpr : cu.findAll(MethodCallExpr.class)) {
                        if (!(exceptionFinder.checkInTry(methodCallExpr)) &&
                                exceptionFinder.checkInFinally(methodCallExpr) &&
                                exceptionFinder.checkThrowException(methodCallExpr)){

                            tmp_data = new String[]{file.getAbsolutePath(),
                                    String.valueOf(methodCallExpr.getRange().get().begin.line),
                                    String.valueOf(methodCallExpr.getRange().get().begin.column),
                                    String.valueOf(methodCallExpr.getRange().get().end.line),
                                    String.valueOf(methodCallExpr.getRange().get().end.column),
                                    methodCallExpr.resolve().getPackageName(),
                                    methodCallExpr.resolve().getClassName(),
                                    methodCallExpr.resolve().getSignature()
                            };
                            saveResultToDisk.writeDataOneByOne(Constant.TESTRESULT, tmp_data);
                            data.add(tmp_data);
                            logger.info("Found anti-pattern: throw exception within finally: {}, {}",
                                    methodCallExpr.resolve().getQualifiedSignature());
                            logger.info("Position of the anti pattern: {}", methodCallExpr.getRange());
                        }

                        if (exceptionFinder.checkThrowMultiExceptions(methodCallExpr)) {

                            tmp_data2 = new String[]{file.getAbsolutePath(),
                                    String.valueOf(methodCallExpr.getRange().get().begin.line),
                                    String.valueOf(methodCallExpr.getRange().get().begin.column),
                                    String.valueOf(methodCallExpr.getRange().get().end.line),
                                    String.valueOf(methodCallExpr.getRange().get().end.column),
                                    methodCallExpr.resolve().getPackageName(),
                                    methodCallExpr.resolve().getClassName(),
                                    methodCallExpr.resolve().getSignature()
                            };
                            saveResultToDisk.writeDataOneByOne(Constant.TESTRESULT2, tmp_data2);
                            data.add(tmp_data2);
                            logger.info("Found anti-pattern: Throw Kitchen Sink: {}, {}",
                                    methodCallExpr.resolve().getQualifiedSignature());
                            logger.info("Position of the anti pattern: {}", methodCallExpr.getRange());
                        }
                    }
                }

                else if(cuNoJar != null) {

                    for (MethodCallExpr methodCallExpr : cuNoJar.findAll(MethodCallExpr.class)) {
                        if (!(exceptionFinder.checkInTry(methodCallExpr)) &&
                                exceptionFinder.checkInFinally(methodCallExpr) &&
                                exceptionFinder.checkThrowException(methodCallExpr)){

                            tmp_data = new String[]{file.getAbsolutePath(),
                                    String.valueOf(methodCallExpr.getRange().get().begin.line),
                                    String.valueOf(methodCallExpr.getRange().get().begin.column),
                                    String.valueOf(methodCallExpr.getRange().get().end.line),
                                    String.valueOf(methodCallExpr.getRange().get().end.column),
                                    methodCallExpr.resolve().getPackageName(),
                                    methodCallExpr.resolve().getClassName(),
                                    methodCallExpr.resolve().getSignature()
                            };
                            saveResultToDisk.writeDataOneByOne(Constant.TESTRESULT, tmp_data);
                            data.add(tmp_data);
                            logger.info("Found anti-pattern: throw exception within finally: {}, {}",
                                    methodCallExpr.resolve().getQualifiedSignature());
                            logger.info("Position of the anti pattern: {}", methodCallExpr.getRange());
                        }

                        if (exceptionFinder.checkThrowMultiExceptions(methodCallExpr)) {

                            tmp_data2 = new String[]{file.getAbsolutePath(),
                                    String.valueOf(methodCallExpr.getRange().get().begin.line),
                                    String.valueOf(methodCallExpr.getRange().get().begin.column),
                                    String.valueOf(methodCallExpr.getRange().get().end.line),
                                    String.valueOf(methodCallExpr.getRange().get().end.column),
                                    methodCallExpr.resolve().getPackageName(),
                                    methodCallExpr.resolve().getClassName(),
                                    methodCallExpr.resolve().getSignature()
                            };
                            saveResultToDisk.writeDataOneByOne(Constant.TESTRESULT2, tmp_data2);
                            data.add(tmp_data2);
                            logger.info("Found anti-pattern: Throw Kitchen Sink: {}, {}",
                                    methodCallExpr.resolve().getQualifiedSignature());
                            logger.info("Position of the anti pattern: {}", methodCallExpr.getRange());
                        }
                    }
                }
            }catch (Exception e){
                logger.error("Unexpected exception occurred.");
            }


        }
    }

}

