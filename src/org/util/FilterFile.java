package org.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class FilterFile {

	/**
     * walk through the path and return all the java files
     * @param path
     * @return a list of paths of java files in this folder
	 */
	public List<File> walk(String path){
		File dir = new File(path);
		Collection<File> files = FileUtils.listFiles(dir, null, true);
		List<File> javaFiles = new ArrayList<File>();
		String extension;
		for(File file :files){
			extension = FilenameUtils.getExtension(file.getName());
			// include all the files in the path except the test files
			if(extension.equals("java") &&
                    !file.getAbsolutePath().contains("schema/SchemaKeyspace.java")
                    && !file.getAbsolutePath().contains("/reflect/TypeToken.java")
                    && !file.getAbsolutePath().contains("schema/LegacySchemaMigrator.java")
                    && !file.getAbsolutePath().contains("service/StorageProxy.java")
                    && !file.getAbsolutePath().contains("/thrift/")
            )

				javaFiles.add(file);
		}
		return javaFiles;
	}
}
