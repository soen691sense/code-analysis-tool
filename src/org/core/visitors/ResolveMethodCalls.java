package org.core.visitors;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JarTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import org.e.detection.execute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class ResolveMethodCalls {
    private static Logger logger = LoggerFactory.getLogger(execute.class);

    public CompilationUnit getCu(File file, File searchPath, String jarPath) {
        try {
        TypeSolver reflectionTypeSolver = new ReflectionTypeSolver();
        TypeSolver jarTypeSolver = new JarTypeSolver(jarPath);
        TypeSolver javaParserTypeSolver = new JavaParserTypeSolver(searchPath);
        reflectionTypeSolver.setParent(reflectionTypeSolver);

        CombinedTypeSolver combinedSolver = new CombinedTypeSolver();
        combinedSolver.add(reflectionTypeSolver);
        combinedSolver.add(javaParserTypeSolver);
        combinedSolver.add(jarTypeSolver);

        JavaSymbolSolver symbolSolver = new JavaSymbolSolver(combinedSolver);
        StaticJavaParser.getConfiguration().setSymbolResolver(symbolSolver);

            return StaticJavaParser.parse(file);
        }catch (Exception e){
            return null;
        }

    }
    
    public CompilationUnit getCuWithNoJar(File file, File searchPath) {
        try {
        TypeSolver reflectionTypeSolver = new ReflectionTypeSolver();
      //  TypeSolver jarTypeSolver = new JarTypeSolver(jarPath);
        TypeSolver javaParserTypeSolver = new JavaParserTypeSolver(searchPath);
        reflectionTypeSolver.setParent(reflectionTypeSolver);

        CombinedTypeSolver combinedSolver = new CombinedTypeSolver();
        combinedSolver.add(reflectionTypeSolver);
        combinedSolver.add(javaParserTypeSolver);
      //  combinedSolver.add(jarTypeSolver);

        JavaSymbolSolver symbolSolver = new JavaSymbolSolver(combinedSolver);
        StaticJavaParser.getConfiguration().setSymbolResolver(symbolSolver);

            return StaticJavaParser.parse(file);
        }catch (Exception e){
            return null;
        }

    }

}
