package org.core.patterns;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.TryStmt;
import org.e.detection.execute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class ExceptionFinder {
    private static Logger logger = LoggerFactory.getLogger(execute.class);

    public boolean checkInTry(MethodCallExpr methodCallExpr) {
        boolean inTryBlock = false;
        try {
            // check whether the node is inside a try statement
            Optional<TryStmt> tryParent = methodCallExpr.findAncestor(TryStmt.class);
            // if so, check whether that try statement has a finally block and if that block contains our node
            if (tryParent.isPresent()) {
                BlockStmt tryBlock = tryParent.get().getTryBlock();
                    Optional<Node> sameNode = tryBlock.findFirst(Node.class, n -> n == methodCallExpr);
                    if (sameNode.isPresent()) {
                        inTryBlock = true;
                    }
            }
        } catch (Exception e) {
//            logger.info("good job");
            return inTryBlock;
        }
        return inTryBlock;
    }

    public boolean checkInFinally(MethodCallExpr methodCallExpr) {
        boolean inFinallyBlock = false;
        try {
            // check whether the node is inside a try statement
            Optional<TryStmt> tryParent = methodCallExpr.findAncestor(TryStmt.class);
            // if so, check whether that try statement has a finally block and if that block contains our node
            while (tryParent.isPresent()) {
                Optional<BlockStmt> finallyBlock = tryParent.get().getFinallyBlock();
                if (finallyBlock.isPresent()) {
                    Optional<Node> sameNode = finallyBlock.get().findFirst(Node.class, n -> n == methodCallExpr);
                    if (sameNode.isPresent()) {
                        inFinallyBlock = true;
                    }
                }
                tryParent = tryParent.get().findAncestor(TryStmt.class);
            }
        } catch (Exception e) {
            return inFinallyBlock;
        }
        return inFinallyBlock;
    }


    public boolean checkThrowException(MethodCallExpr methodCallExpr) {
        try {
            boolean hasThrowException = false;
       
            Optional<MethodDeclaration> methodDeclaration = methodCallExpr.resolve().toAst();
            if (methodDeclaration.isPresent()) {
                if (methodDeclaration.get().getThrownExceptions().size() > 0) {
                    	hasThrowException = true;        
                }
            }
            return hasThrowException;
        }catch (Exception e){
//            logger.info("Good job..");
            return  false;
        }
    }
    
    /*
     * Check whether method include multiple exceptions
     * The Throwing the Kitchen Sink anti-pattern
     * multiple checked exceptions should wrap them in a single checked exception
     */
    public boolean checkThrowMultiExceptions(MethodCallExpr methodCallExpr) {
        try {
        	boolean ThrowMultiExceptions = false;
        	
            Optional<MethodDeclaration> methodDeclaration = methodCallExpr.resolve().toAst();
            if (methodDeclaration.isPresent()) {
               if (methodDeclaration.get().getThrownExceptions().size() > 2) {
            	// Throwing Exception anti pattern detect   if(methodDeclaration.toString().trim().equals(new String("Exception"))) {}
            	   ThrowMultiExceptions = true;
                }
            }
            return ThrowMultiExceptions;
        }catch (Exception e){
//            logger.info("Good job..");
            return  false;
        }
    }


}
